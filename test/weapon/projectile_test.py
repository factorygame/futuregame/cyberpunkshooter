from test.template.template_gui import GuiTest
# from factorygame.core.engine_base import GameEngine
from factorygame.core.blueprint import WorldGraph
from factorygame.utils.gameplay import GameplayStatics, GameplayUtilities
from factorygame.utils.loc import Loc
from cyberpunk.cyberpunk_shooter import CyberpunkEngine

# Import the projectile to be tested.
from cyberpunk.weapon.projectile import Projectile

from tkinter.ttk import Label

class TestEngine(CyberpunkEngine):
    def __init__(self):
        super().__init__()
        self._starting_world = WorldGraph

class ProjectileMovementTest(GuiTest):
    _test_name = "CYBERPUNK Projectile Movement"

    def start(self):

        Label(self, text="Projectiles are launched in a direction and "\
            "continue for...\never!").pack()

        # Create the base class engine in this Toplevel window.
        game_engine = GameplayUtilities.create_game_engine(
            TestEngine, master=self)

        # Spawn projectile at the world origin.
        my_weapon = GameplayStatics.world.spawn_actor(Projectile, (0, 0))
        my_weapon.launch(Loc(1, 0))

        # Ensure we stop the game engine when closing the test,
        # so that subsequent runs are fully restarted.
        self.bind("<Destroy>", self.on_destroy)

    def on_destroy(self, event):
        """Called when test window is destroyed."""
        GameplayUtilities.close_game()
