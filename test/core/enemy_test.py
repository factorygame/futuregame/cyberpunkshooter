from test.template.template_gui import GuiTest
# from factorygame.core.engine_base import GameEngine
from factorygame.core.blueprint import WorldGraph, PolygonNode
from factorygame.utils.gameplay import GameplayStatics, GameplayUtilities
from cyberpunk.cyberpunk_shooter import CyberpunkEngine

# Import the player to be tested.
from cyberpunk.core.player import CyberpunkEnemy

from tkinter.ttk import Label

class TestEngine(CyberpunkEngine):
    def __init__(self):
        super().__init__()
        self._starting_world = WorldGraph

class TestEnemy(CyberpunkEnemy):
    def tick(self, delta_time):
        self.rotation += delta_time * 0.01

        super(PolygonNode, self).tick(delta_time)

    def on_click(self, event):
        self.location += 100

class TestMovingEnemy(CyberpunkEnemy):
    pass


class EnemySpawnTest(GuiTest):
    _test_name = "CYBERPUNK Enemy Spawn"

    def start(self):

        Label(self, text="The game slows down when adding lots of enemies.").pack()
        Label(self, text="This is about the limit to keep playability.").pack()

        # Create the base class engine in this Toplevel window.
        game_engine = GameplayUtilities.create_game_engine(
            TestEngine, master=self)

        # Spawn enemies in a 2d grid.
        for i in range(4):
            for j in range(4):
                GameplayStatics.world.spawn_actor(
                    TestEnemy, (300 * i, 300 * j))

        # Ensure we stop the game engine when closing the test,
        # so that subsequent runs are fully restarted.
        self.bind("<Destroy>", self.on_destroy)

    def on_destroy(self, event):
        """Called when test window is destroyed."""
        GameplayUtilities.close_game()


class EnemyMovementTest(GuiTest):
    _test_name = "CYBERPUNK Enemy Movement"

    def start(self):

        Label(self, text="Enemies move randomly, then wait at a point.").pack()
        Label(self, text="They should be bound to a level area.").pack()

        # Create the base class engine in this Toplevel window.
        game_engine = GameplayUtilities.create_game_engine(
            TestEngine, master=self)

        # Spawn an enemy at the origin.
        GameplayStatics.world.spawn_actor(TestMovingEnemy, (0, 0))

        # Ensure we stop the game engine when closing the test,
        # so that subsequent runs are fully restarted.
        self.bind("<Destroy>", self.on_destroy)

    def on_destroy(self, event):
        """Called when test window is destroyed."""
        GameplayUtilities.close_game()
