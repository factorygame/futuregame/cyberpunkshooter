from test.template.template_gui import GuiTest
from factorygame.core.engine_base import GameEngine
from factorygame.utils.gameplay import GameplayStatics, GameplayUtilities
from factorygame.core.blueprint import WorldGraph, FColor, PolygonNode, GeomHelper

from tkinter.ttk import Button, Label

class TestTriangle(PolygonNode):

    def begin_play(self):
        super().begin_play()

        self.vertices = list(GeomHelper.generate_reg_poly(3, radius=200))
        self.fill_color = FColor.cyan()

class TestWorld(WorldGraph):
    """The world for a single instance of hacking."""

    def __init__(self):
        super().__init__()

        self.config(background=FColor(15).to_hex())

    def begin_play(self):
        super().begin_play()

        self.spawn_actor(TestTriangle, (200, 200))

        browse_btn = Button(self, text="Browse World Again",
            command=self.show_jump_text)
        self.create_window(10, 10, window=browse_btn, anchor="nw")

    def show_jump_text(self):
        GameplayUtilities.travel(TestWorld)


class TestGameEngine(GameEngine):
    


class WorldTravelTest(GuiTest):
    _test_name = "World Travel"

    def start(self):

        Label(self, text="Press \"Browse World Again\" to reload "\
            "the world.").pack()

        # Create the base class engine in this Toplevel window.
        game_engine = GameplayUtilities.create_game_engine(GameEngine, master=self)

        
        # Ensure we stop the game engine when closing the test, 
        # so that subsequent runs are fully restarted.
        self.bind("<Destroy>", self.on_destroy)

    def on_destroy(self, event):
        """Called when test window is destroyed."""
        GameplayUtilities.close_game()


