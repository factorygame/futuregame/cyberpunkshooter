from test.template.template_gui import GuiTest
# from factorygame.core.engine_base import GameEngine
from factorygame.core.blueprint import WorldGraph
from factorygame.utils.gameplay import GameplayStatics, GameplayUtilities
from cyberpunk.cyberpunk_shooter import CyberpunkEngine

# Import the player to be tested.
from cyberpunk.core.player import CyberpunkPlayer

from tkinter.ttk import Label

class TestEngine(CyberpunkEngine):
    def __init__(self):
        super().__init__()
        self._starting_world = WorldGraph

class PlayerMovementTest(GuiTest):
    _test_name = "CYBERPUNK Player Movement"

    def start(self):

        Label(self, text="Use \"W-A-S-D\" to move.").pack()
        Label(self, text="Use \"I-J-K-L\" to aim.").pack()

        # Create the base class engine in this Toplevel window.
        game_engine = GameplayUtilities.create_game_engine(
            TestEngine, master=self)

        # Spawn player at the world origin.
        GameplayStatics.world.spawn_actor(CyberpunkPlayer, (0, 0))

        # Ensure we stop the game engine when closing the test,
        # so that subsequent runs are fully restarted.
        self.bind("<Destroy>", self.on_destroy)

    def on_destroy(self, event):
        """Called when test window is destroyed."""
        GameplayUtilities.close_game()
