# Cyberpunk Shooter

A 2d geometric hacking minigame to take down the mega corporations
of cyberpunk city.

## Engine

Uses the open source Python engine called **Factoryengine**.

This open source engine can be downloaded for free. The source code is
available here:
[https://gitlab.com/factorygame/factorygame](https://gitlab.com/factorygame/factorygame)

Note that this repository already contains a copy of the engine in the
`factorygame` module. This allows the engine to modified where necessary.

## Source Control Notice

This project is a fork of Factoryengine for simplicity. Note that all commits
prior to *Sep 27, 2019 6PM GMT* were entirely related to the game engine.

This game is build around the engine module (with a few modifications). Any
new game specific code is added to the `cyberpunk` module and the `test`
module.
