from factorygame.core.engine_base import GameEngine
from factorygame.core.input_base import EKeys
from cyberpunk.core.world import HackingWorld
from cyberpunk.menus.start_world import StartingWorld

import winsound


class CyberpunkEngine(GameEngine):
    """Game engine class for Cyberpunk Shooter."""

    def __init__(self):

        # Set default properties.
        self._window_title = "Cyberpunk Shooter"
        self._frame_rate = 90
        self._starting_world = StartingWorld

        # Start the menu music
        filename = "cyberpunk/music/factory.wav"
        winsound.PlaySound(filename,
            winsound.SND_FILENAME | winsound.SND_LOOP | winsound.SND_ASYNC)

    def setup_input_mappings(self):
        """Set gameplay input mappings."""

        # Player directional movement.
        self.input_mappings.add_action_mapping("MoveForward",   EKeys.W)
        self.input_mappings.add_action_mapping("MoveLeft",      EKeys.A)
        self.input_mappings.add_action_mapping("MoveBack",      EKeys.S)
        self.input_mappings.add_action_mapping("MoveRight",     EKeys.D)

        # Player keyboard based direction aiming.
        self.input_mappings.add_action_mapping("AimForward",   EKeys.I)
        self.input_mappings.add_action_mapping("AimLeft",      EKeys.J)
        self.input_mappings.add_action_mapping("AimBack",      EKeys.K)
        self.input_mappings.add_action_mapping("AimRight",     EKeys.L)

        # Shooting the gun.
        # A bit weird button mapping but SpaceBar isn't recognised by factorygame.
        self.input_mappings.add_action_mapping("Shoot", EKeys.Q, EKeys.O,
            EKeys.LeftMouseButton)
            
        # Fire when the "aim" key is released.
        self.input_mappings.add_action_mapping(
            "AimShoot", EKeys.RightMouseButton)
