from factorygame.core.blueprint import WorldGraph, FColor
from factorygame.utils.gameplay import GameplayUtilities, GameplayStatics
from cyberpunk.core.world import HackingWorld

from tkinter.ttk import Button

import winsound

class StartingWorld(WorldGraph):
    """The world of start menu."""

    def __init__(self):
        super().__init__()

        self.config(bg=FColor(15).to_hex())

    def begin_play(self):
        btn = Button(self, text="/usr/bin/initiate_hacking.py",
            command=self.on_start_game)
        self.create_window((10, 0), anchor="nw", window=btn)

        self.create_text((10, 30), text="Cyberpunk Shooter", anchor="nw",
            font=("Comic Sans MS", "24"), fill=FColor.red().to_hex())

        self.create_text((10, 100), text="Creator:\nDavid Kanekanian", anchor="nw",
            font=("Comic Sans MS", "12"), fill=FColor.cyan().to_hex())

        self.create_text((10, 170), text="Open Source Asset: Factory.wav:\n"\
            "https://opengameart.org/content/factory-ambiance", anchor="nw",
            font=("Comic Sans MS", "12"), fill=FColor.cyan().to_hex())

        self.create_text((10, 220), text="Open Source Asset: Ruskerdax - " \
            "Pondering the Cosmos.wav:\n"\
            "https://opengameart.org/content/ruskerdax-pondering-the-cosmos-0",
            anchor="nw",
            font=("Comic Sans MS", "12"), fill=FColor.cyan().to_hex())

        self.focus_set()

        # Set initial window size.
        GameplayStatics.root_window.geometry("1280x720")

    def on_start_game(self):
        # Start the actual game music (persists between world travel)
        filename = "cyberpunk/music/ruskerdax_pondering_the_cosmos.wav"
        winsound.PlaySound(filename,
            winsound.SND_FILENAME | winsound.SND_LOOP | winsound.SND_ASYNC)
        
        GameplayUtilities.travel(HackingWorld)
