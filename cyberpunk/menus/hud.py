
from factorygame.core.blueprint import DrawnActor, FColor
from factorygame.core.engine_base import ETickGroup

class Hud(DrawnActor):
    """Displays text"""

    def __init__(self):
        super().__init__()

        self.primary_actor_tick.tick_group = ETickGroup.UI        

    def _draw(self):

        # Show the kill counter in the top right screen corner.

        text = ""
        font_size = int(30 * 1/self.world.get_screen_size_factor())

        if self.world.gamemode.is_player_dead:
            # Show num kills
            text = "You Lose, %d total kills" % self.world.gamemode.player_kill_count
            font_size -= 20
            font_size *= 5  # REALLY BIG TEXT! - can be laggy for big screens
            self.world.create_text(
                self.world.get_canvas_dim() // 2 + (0, -50),
                text=text,
                font=("Comic Sans MS", font_size), fill=FColor.red().to_hex(),
                tags=(self.unique_id))

        else:
            text = "Kills: %d" % self.world.gamemode.player_kill_count
            font_size -= 10
            self.world.create_text((10, 50), anchor="nw",
                text=text, font=("Comic Sans MS", font_size),
                fill=FColor.white().to_hex(), tags=(self.unique_id))
