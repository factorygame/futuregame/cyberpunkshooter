
from factorygame.core.engine_base import Actor, ETickGroup
from cyberpunk.core.player import RandomSizeEnemy

import random

class CyberpunkHackingGameMode(Actor):
    """
    Handle hacking minigame rules and game state.
    """

    def __init__(self):
        super().__init__()

        ## Time to the next cycle of spawning enemies.
        self.time_to_next_enemy_wave = 2.0

        # Randomness
        self.time_to_next_enemy_wave_min = 5.0
        self.time_to_next_enemy_wave_max = 10.0

        ## Current active_enemies.
        self.active_enemies = []

        ## Point at which we must spawn more enemies.
        self.min_num_active_enemies = 2
        self.max_num_active_enemies = 10  # Doesn't do anything!

        ## Amount of enemies to spawn at a single moment.
        self.wave_spawn_amount_min = 3
        self.wave_spawn_amount_max = 7


        self.primary_actor_tick.tick_group = ETickGroup.WORLD

        ## Number of enemies player has killed.
        self.player_kill_count = 0

        ## Whether player is dead.
        self.is_player_dead = False

   # # # # # # # # # # # # # # # # # #
   # Start of actor interface.

    def begin_play(self):
        super().begin_play()

    def tick(self, delta_time):
        if len(self.active_enemies) < self.min_num_active_enemies:

            delta_seconds = delta_time * 0.001
            self.time_to_next_enemy_wave -= delta_seconds
            if self.time_to_next_enemy_wave <= 0:
                self.spawn_wave()

   # End of actor interface.
   # # # # # # # # # # # # # # # # # #

    def spawn_wave(self):
        """
        Spawn a wave of enemies.
        """

        num_to_spawn = int(RandomSizeEnemy.random_range(
            self.wave_spawn_amount_min,
            self.wave_spawn_amount_max))

        player_loc = self.world.player.location
        spawn_loc_min = player_loc - 1000
        spawn_loc_max = player_loc + 1000
        for i in range(num_to_spawn):
            spawn_location = (
                RandomSizeEnemy.random_range(spawn_loc_min.x, spawn_loc_max.x),
                RandomSizeEnemy.random_range(spawn_loc_min.y, spawn_loc_max.y))

            self.world.spawn_actor(RandomSizeEnemy, spawn_location)

        # Schedule the next wave!
        self.time_to_next_enemy_wave = RandomSizeEnemy.random_range(
            self.time_to_next_enemy_wave_min,
            self.time_to_next_enemy_wave_max)
