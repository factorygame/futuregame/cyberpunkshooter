
# from factorygame.core.engine_base import Actor

# class MyActor(Actor):
#     """
#     Description of MyActor.
#     """

#     def __init__(self):
#         super().__init__()

#    # # # # # # # # # # # # # # # # # #
#    # Start of actor interface.

#     def begin_play(self):
#         super().begin_play()

#     def tick(self, delta_time):
#         super().tick(delta_time)

#    # End of actor interface.
#    # # # # # # # # # # # # # # # # # #

from factorygame.core.engine_base import Actor
from factorygame.core.blueprint import PolygonNode, GeomHelper, FColor
from factorygame.core.input_base import EInputEvent
from factorygame.utils.gameplay import GameplayStatics
from factorygame.utils.loc import Loc
from factorygame.utils.mymath import MathStat

import math
import random


class CyberpunkPawn(PolygonNode):
    """Base class for players."""

    def __init__(self):
        super().__init__()

        ## Rotation of rectangle, in radians.
        self._rotation = 0

        ## Direction to move player.
        self.movement_target = Loc(0, 0)

        ## Direction of aim.
        self.aim_target = Loc(0, 0)

        ## Speed to move player at per second, in world units.
        self.move_speed = 200

        ## The gun that I use to shoot enemies.
        self.gun = None

    def give_gun(self, in_gun):
        """
        Set the active gun for this pawn.

        :param in_gun: (Gun) Active gun actor.
        """
        
        if self.gun is not None:
            # Do something with the old gun.
            self.gun.owner = None

        # Assign the new gun to... ME!
        self.gun = in_gun
        self.gun.owner = self

   # # # # # # # # # # # # # # # # # #
   # Start of actor interface.

    def begin_play(self):
        super().begin_play()

        # Calculate initial player vertices.
        self.set_verts_from_rotation(0)

    def tick(self, delta_time):
        super().tick(delta_time)

        delta_seconds = 1/delta_time
        self.location += self.movement_target * delta_seconds * self.move_speed

        # Smooth out rotation slightly.
        # TODO: Find shortest rotation path (when to loop through 0)
        target_rotation = math.atan2(*self.aim_target)
        last_rotation = self.rotation
        new_rotation = MathStat.lerp(last_rotation, target_rotation, 0.4)
        self.rotation = new_rotation
        # self.rotation = target_rotation

   # End of actor interface.
   # # # # # # # # # # # # # # # # # #

    def set_verts_from_rotation(self, in_rotation):
        """
        Rotate the player vertices to the given amount.
        
        :param in_rotation: New rotation, in radians.
        """
        raise NotImplementedError("Pawn '%s' must implement "
                                  "set_verts_from_rotation" % type(self).__name__)

    @property
    def rotation(self):
        return self._rotation

    @rotation.setter
    def rotation(self, value):
        self.set_verts_from_rotation(value)
        self._rotation = value


class CyberpunkPlayer(CyberpunkPawn):
    """
    Player for Cyberpunk Shooter game.
    """

    def __init__(self):
        super().__init__()

        # Make player neon red.
        self.fill_color = FColor.red()

    def set_verts_from_rotation(self, in_rotation):

        octagon = GeomHelper.generate_reg_poly(
            8, radius=100, radial_offset=in_rotation)

        # Only pick the "side" verts to make a rectangle.
        allowed_verts = {0, 1, 4, 5}
        new_verts = [vertex for i, vertex in enumerate(
            octagon) if i in allowed_verts]
        self.vertices = new_verts

    # # # # # # # # # # # # # # # # # #
    # Start of input handling.

    def setup_input_component(self, input_component):
        """
        Bind actions to the engine's input component.
        """

        # TODO: Add axis input bindings.

        # Bind to set movement target.

        input_component.bind_action(
            "MoveForward", EInputEvent.PRESSED, self.on_move_forward)
        input_component.bind_action(
            "MoveBack",     EInputEvent.PRESSED, self.on_move_back)
        input_component.bind_action(
            "MoveRight",    EInputEvent.PRESSED, self.on_move_right)
        input_component.bind_action(
            "MoveLeft",     EInputEvent.PRESSED, self.on_move_left)

        input_component.bind_action(
            "MoveForward", EInputEvent.RELEASED, self.on_end_move_forward)
        input_component.bind_action(
            "MoveBack",     EInputEvent.RELEASED, self.on_end_move_back)
        input_component.bind_action(
            "MoveRight",    EInputEvent.RELEASED, self.on_end_move_right)
        input_component.bind_action(
            "MoveLeft",     EInputEvent.RELEASED, self.on_end_move_left)

        # Bind to set aim target.

        input_component.bind_action(
            "AimForward", EInputEvent.PRESSED, self.on_aim_forward)
        input_component.bind_action(
            "AimBack",     EInputEvent.PRESSED, self.on_aim_back)
        input_component.bind_action(
            "AimRight",    EInputEvent.PRESSED, self.on_aim_right)
        input_component.bind_action(
            "AimLeft",     EInputEvent.PRESSED, self.on_aim_left)

        input_component.bind_action(
            "AimForward", EInputEvent.RELEASED, self.on_end_aim_forward)
        input_component.bind_action(
            "AimBack",     EInputEvent.RELEASED, self.on_end_aim_back)
        input_component.bind_action(
            "AimRight",    EInputEvent.RELEASED, self.on_end_aim_right)
        input_component.bind_action(
            "AimLeft",     EInputEvent.RELEASED, self.on_end_aim_left)


        # Bind to SHOOT!

        input_component.bind_action(
            "Shoot", EInputEvent.PRESSED, self.on_shoot)
        input_component.bind_action(
            "AimShoot", EInputEvent.RELEASED, self.on_shoot)

    # Functions to set move target.

    def on_move_forward(self, amount=1):
        self.movement_target.y = amount

    def on_move_back(self, amount=1):
        self.movement_target.y = -amount

    def on_move_right(self, amount=1):
        self.movement_target.x = amount

    def on_move_left(self, amount=1):
        self.movement_target.x = -amount

    def on_end_move_forward(self):
        if self.movement_target.y == 1:
            self.movement_target.y = 0

    def on_end_move_back(self):
        if self.movement_target.y == -1:
            self.movement_target.y = 0

    def on_end_move_right(self):
        if self.movement_target.x == 1:
            self.movement_target.x = 0

    def on_end_move_left(self):
        if self.movement_target.x == -1:
            self.movement_target.x = 0

    # Functions to set aim target.

    def on_aim_forward(self, amount=1):
        self.aim_target.y = amount

    def on_aim_back(self, amount=1):
        self.aim_target.y = -amount

    def on_aim_right(self, amount=1):
        self.aim_target.x = amount

    def on_aim_left(self, amount=1):
        self.aim_target.x = -amount

    # End movement functions are only called from keyboard
    # rotation keys.

    def on_end_aim_forward(self):
        if self.aim_target.y == 1:
            self.aim_target.y = 0

    def on_end_aim_back(self):
        if self.aim_target.y == -1:
            self.aim_target.y = 0

    def on_end_aim_right(self):
        if self.aim_target.x == 1:
            self.aim_target.x = 0

    def on_end_aim_left(self):
        if self.aim_target.x == -1:
            self.aim_target.x = 0


    # Anything that isn't MOVING code!!!!!

    def on_shoot(self):
        if self.world.gamemode.is_player_dead:
            # This object should be garbage collected by now,
            # but it isn't because of too many references!

            return

        if self.gun is not None:
            self.gun.shoot()

    # End of input handling.
    # # # # # # # # # # # # # # # # # #

    # # # # # # # # # # # # # # # # # #
    # Start of actor interface.

    def begin_play(self):
        super().begin_play()

        input_comp = GameplayStatics.game_engine.input_mappings
        self.setup_input_component(input_comp)

    def begin_destroy(self):
        super().begin_destroy()

        self.world.gamemode.is_player_dead = True

    def tick(self, delta_time):
        super().tick(delta_time)

        # Align viewport to player position.
        self.world._view_offset = self.location

    # End of actor interface.
    # # # # # # # # # # # # # # # # # #


class CyberpunkEnemy(CyberpunkPawn):
    """Base class for enemies."""

    def __init__(self):
        super().__init__()

        ## Initial amount of protection against attacks.
        self.max_health = 100  # Doesn't do anything: dies on any impact

        ## Amount of protection against attacks.
        self.health = None

        ## Number of vertices for shape to have. More vertices
        ## means a stronger enemy. Must be at least 3.
        self.num_verts = 3

        ## Radius of shape, in world units.
        ## Bigger radius means a stronger enemy.
        self.radius = 100

        self.fill_color = FColor.yellow()

        ## Amount of time to stay at a spot before moving again, in seconds.
        self.wait_at_point_duration_min = 3.0
        self.wait_at_point_duration_max = 6.0

        ## Amount of time to spend moving in a single direction, in seconds.
        self.move_direction_duration_min = 1
        self.move_direction_duration_max = 1.2

        self._next_move_func = None

        ## Used to see when we are ready to move.
        self._time_counter = 0.0

    def init_gameplay_attributes(self):
        """
        Called once right after spawn to initialise
        attributes for gameplay.

        :warning: To set defaults, use the constructor instead.
        """

        self.health = self.max_health

        self._next_move_func = self.move_to_random_point

    def set_verts_from_rotation(self, in_rotation):

        vert_generator = GeomHelper.generate_reg_poly(
            self.num_verts, radius=self.radius, radial_offset=in_rotation)

        self.vertices = tuple(vert_generator)

    # # # # # # # # # # # # # # # # # #
    # Start of actor interface.

    def begin_play(self):
        super().begin_play()

        self.init_gameplay_attributes()

        self.world.give_enemy_gun(self)

    def tick(self, delta_time):
        super().tick(delta_time)
        delta_seconds = delta_time * 0.001
        self._time_counter -= delta_seconds

        if self._time_counter <= 0:
            self._next_move_func.__call__()
            self.schedule_next_move()

    # End of actor interface.
    # # # # # # # # # # # # # # # # # #

    def wait_at_point(self):
        """Wait until told to move again."""
        # Stop moving.
        self.movement_target = Loc(0, 0)
        
        # Shoot the player.
        self.shoot_towards_player()
        
    def shoot_towards_player(self):
        """
        Turn to face the player then take a shot at him.
        """

        if self.gun is None: return

        # Turn to face the player.
        player = self.world.player
        if player is not None:

            direction = player.location - self.location
            # Make it a unit vector
            direction /= abs(direction)

            self.aim_target = direction

        # Also shoot my gun.
        # It'll go off in whatever direction I am facing.
        self.gun.shoot()

    def move_to_random_point(self):
        """
        Set the movement target to a random direction.
        """
        # Pick a random direction to move in.
        new_direction = Loc(
            self.random_range(-.5, .5), self.random_range(-.5, .5))

        # Also face the way I'm moving.
        self.movement_target = new_direction
        self.aim_target = new_direction.copy()

    def schedule_next_move(self):
        # Schedule the next move.
        if self._next_move_func == self.wait_at_point:
            self._next_move_func = self.move_to_random_point
            self._time_counter = self.random_range(
                self.wait_at_point_duration_min,
                self.wait_at_point_duration_max)
            pass

        else:
            self._next_move_func = self.wait_at_point
            self._time_counter = self.random_range(
                self.move_direction_duration_min,
                self.move_direction_duration_max)

    def random_range(self, min=0, max=1):
        """
        Return a random real number between min and max.
        """

        return MathStat.map_range(random.random(), 0, 1, min, max)

class RandomSizeEnemy(CyberpunkEnemy):
    """
    Enemy that spawns with random number of vertices.
    """

    def __init__(self):
        super().__init__()

        self.num_verts = random.randint(3, 7)

        colors = [FColor().yellow(), FColor().green(),
            FColor.blue(), FColor.cyan(), FColor.magenta()]
        self.fill_color = random.choice(colors)

        self.radius = self.random_range(80, 300)




