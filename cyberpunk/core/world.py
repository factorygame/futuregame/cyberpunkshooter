from factorygame.core.blueprint import WorldGraph, FColor, PolygonNode, GeomHelper
from factorygame.utils.gameplay import GameplayUtilities
from factorygame.utils.loc import Loc
from cyberpunk.core.player import CyberpunkPlayer, CyberpunkEnemy
from cyberpunk.core.gamemode import CyberpunkHackingGameMode
from cyberpunk.weapon.gun import Gun
from cyberpunk.menus.hud import Hud

from tkinter.ttk import Button

class TestTriangle(PolygonNode):

    def begin_play(self):
        super().begin_play()

        self.vertices = list(GeomHelper.generate_reg_poly(3, radius=200))
        self.fill_color = FColor.cyan()

class HackingWorld(WorldGraph):
    """The world for a single instance of hacking."""

    def __init__(self):
        super().__init__()

        self.config(background=FColor(15).to_hex())

        # Makes border slightly darker.
        self.focus_set()

        ## Reference to active player, if any.
        self.player = None

    def begin_play(self):
        super().begin_play()

        tri = self.spawn_actor(TestTriangle, (200, 200))
        tri.location = Loc(-200, 200)

        browse_btn = Button(self, text="Browse World Again",
            command=lambda: GameplayUtilities.travel(HackingWorld))
        self.create_window(0, 0, window=browse_btn, anchor="nw")


        # Spawn the player at the world origin.

        self.player = self.spawn_actor(CyberpunkPlayer, Loc(0, 0))
        
        # Spawn the all important GameMode (mostly for match state tracking).
        self.gamemode = self.spawn_actor(CyberpunkHackingGameMode, Loc(0, 0))
        
        # Spawn player's gun.
        # Location doesn't really matter as it isn't drawn!
        player_gun = self.spawn_actor(Gun, self.player.location)
        self.player.give_gun(player_gun)

        self.spawn_actor(Hud, Loc(0, 0))

    def on_graph_motion_input(self, event):
        # We want the camera aligned to the player's position.
        # Instead of affecting the viewport on right click dragging
        # we can use it to set the player aim.

        if self.player is None: return

        # Normalise the delta to exactly 1, 0 or -1.

        ## Magnitude of delta to ignore.
        deadzone = 0.2

        delta = event.delta
        delta.y *= -1

        if delta.x < deadzone and delta.x > -deadzone:
            delta.x = 0
        if delta.y < deadzone and delta.y > -deadzone:
            delta.y = 0

        # Only call set rotation events.
        # Never go back to normal from mouse input.

        self.player.aim_target = delta

    def give_enemy_gun(self, enemy):
        """
        Give the enemy their gun. Must be called here due to
        circular module import errors in the player module.        
        """
        the_gun = self.spawn_actor(Gun, enemy.location)
        enemy.give_gun(the_gun)
