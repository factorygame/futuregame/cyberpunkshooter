from factorygame.core.blueprint import GeomHelper, FColor
from factorygame.core.engine_base import ETickGroup
from cyberpunk.core.player import CyberpunkPawn

class Projectile(CyberpunkPawn):
    """
    Moving rectangle with a Cyberpunk Shooter

    Make sure you call `launch` to send it off in the initial
    direction
    """

    def __init__(self):
        super().__init__()

        # Make projectiles move real slowly!
        self.move_speed = 50

        self.fill_color = FColor.magenta()

        # Ensure there isn't a collision when spawning on game ticks.
        self.primary_actor_tick.tick_group = ETickGroup.UI

        ## Lifetime of the projectile, in seconds.
        self.lifetime = 2.0

        ## Pawn that owns me.
        self.owner = None

    def launch(self, direction):
        """
        Send the projectile moving forward til it hits something.

        :param direction: (Loc) 2D vector of move direction.
        """
        # Move in the specified direction
        self.movement_target = direction

        # Also keep looking towards our prey.
        self.aim_target = direction

    def set_verts_from_rotation(self, in_rotation):

        octagon = GeomHelper.generate_reg_poly(
            8, radius=25, radial_offset=in_rotation)

        # Only pick the "side" verts to make a "forward facing" rectangle.
        allowed_verts = {2, 3, 6, 7}
        new_verts = [vertex for i, vertex in enumerate(
            octagon) if i in allowed_verts]
        self.vertices = new_verts

    def get_first_colliding_actor(self):
        """
        Return a list of actors that are inside me.
        """
        # Don't consider rotation.
        center = self.location
        half_bounds = 25
        coords = []
        bl = center - half_bounds
        tr = center + half_bounds
        coords.extend(self.world.view_to_canvas(bl))  # Bottom left
        coords.extend(self.world.view_to_canvas(tr))  # Top right

        # Found ids are returned in order of creation, not necessarily what
        # is displayed at the top.
        found_ids = self.world.find_overlapping(*coords)
        for it in found_ids:
            node = self.world.render_manager.node_canvas_ids.get(it)
            if node is not None:
                return node

   # # # # # # # # # # # # # # # # # #
   # Start of actor interface.

    def begin_play(self):
        super().begin_play()

    def tick(self, delta_time):

        delta_seconds = delta_time * 0.001
        self.lifetime -= delta_seconds
        if self.lifetime <= 0:
            self.world.destroy_actor(self)

        other_actor = self.get_first_colliding_actor()
        if other_actor is not None and other_actor is not self.owner:

            # TODO: Actually subtract health with a proper damage system.

            if self.owner is self.world.player:
                self.world.gamemode.player_kill_count += 1

            self.world.destroy_actor(other_actor)
            self.world.destroy_actor(self)

        super().tick(delta_time)

   # End of actor interface.
   # # # # # # # # # # # # # # # # # #

    def begin_destroy(self):
        super().begin_destroy()

        # Tell the gun to remove us from his active projectiles.
        self.owner.gun._active_projectiles.remove(self)
