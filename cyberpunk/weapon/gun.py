from factorygame.core.engine_base import Actor
from cyberpunk.weapon.projectile import Projectile


class Gun(Actor):
    """
    Shooter of projectiles. It doesn't really have a world
    representation by default.
    """

    def __init__(self):
        super().__init__()

        ## Pawn that owns this gun. Responsible for damage dealt from projectiles.
        self.owner = None

        ## Class of projectile to fire.
        self.projectile_class = Projectile

        ## Maximum number of projectiles to be active at once.
        self.max_projectiles = 3

        self._active_projectiles = []

    def shoot(self):
        """
        Shoot a projectile in the current facing direction.
        """

        if self.owner is None:
            raise RuntimeError("Gun cannot fire without an owner.")

        if len(self._active_projectiles) >= self.max_projectiles:
            # Don't shoot more than we're allowed to.
            return

        new_proj = self.world.spawn_actor(
            self.projectile_class, self.owner.location)

        new_proj.owner = self.owner

        # Make a copy of the player's aim target (so the projectile don't
        # continually track it!!!)
        launch_direction = self.owner.aim_target.copy()
        if launch_direction.x == launch_direction.y == 0:
            # This means the player is facing "up".
            # Shoot the projectile up.
            launch_direction.y = 1
        new_proj.launch(launch_direction)

        # Add to the active projectiles list.
        self._active_projectiles.append(new_proj)

   # # # # # # # # # # # # # # # # # #
   # Start of actor interface.

    def begin_play(self):
        super().begin_play()

    def tick(self, delta_time):
        pass

   # End of actor interface.
   # # # # # # # # # # # # # # # # # #
