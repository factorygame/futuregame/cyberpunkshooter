import winsound, base64

filename = input("Enter filename\n>>>")

with open(filename, "rb") as file:
    sound_data = base64.b64encode(file.read())
    sound_data = base64.b64decode(sound_data)

while 1:
    mode = input("Enter sound play mode\n\t%s\t%s\n\t%s\t%s\n>>>" \
        % ("f", "Filename", "d", "Sound Data")).lower()

    if mode == "f":
        winsound.PlaySound(filename, winsound.SND_FILENAME | winsound.SND_LOOP | winsound.SND_ASYNC)
        break

    elif mode == "d":
        import threading, sys
        sound_thread = threading.Thread(target=winsound.PlaySound,
            args=(sound_data, winsound.SND_MEMORY | winsound.SND_LOOP))
        sound_thread.daemon = True
        sound_thread.start()
        break

print("success")

input("Press anything to quit\n>>>")

if "sound_thread" in globals():
    exit(0)
